i3-brightness
=============
[![License: GPL v2](https://img.shields.io/badge/License-GPL%20v2-blue.svg)][license]

Credit goes to the original author of [i3-volume](https://github.com/hastinbe/i3-volume).

This is an adaptation of i3-volume for brightness control with notifications.

## License

`i3-brightness` is released under [GNU General Public License v2][license]

Copyright (C) 1989, 1991 Free Software Foundation, Inc.

[dunst]: https://dunst-project.org
[i3wm]: https://i3wm.org
[libnotify]: https://developer.gnome.org/libnotify
[license]: https://www.gnu.org/licenses/gpl-2.0.en.html
[notify-osd]: https://launchpad.net/notify-osd
